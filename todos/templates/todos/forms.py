from django.forms import ModelForm
from todos.models import TodoList, TodoItem

class CreateList(ModelForm):
    class Meta:
        model = TodoList
        fields =[
            'name',
        ]

class UpdateItem(ModelForm):
    class Meta:
        model = TodoItem
        fields =[
            'task',
            'due_date',
            "is_completed",
            "list",
        ]

class DeleteList(ModelForm):
    class Meta:
        model=TodoList
        fields=[

        ]