from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.Templates.todos.forms import CreateList, UpdateItem, DeleteList

# Create your views here.
def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list": todo_list
    }
    return render(request, 'todos/list.html', context)

def todo_list_detail(request, id):
    todo_list= get_object_or_404(TodoList, id=id)
    context={
        "todo_list": todo_list,
    }
    return render(request, 'todos/detail.html', context)

def todo_list_create(request):
    if request.method == 'POST':
        form = CreateList(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect('todo_list_detail', id = list.id)
    else:
        form = CreateList()
    context={
        'form': form,
    }
    return render(request,'todos/create.html', context)

def todo_list_update(request, id): #This function updates the Name of the list only, does not add items to the list
    post = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        form= CreateList(request.POST, instance=post)
        if form.is_valid():
            form = form.save()
            return redirect('todo_list_detail', id=post.id)
    else:
        form = CreateList(instance=post)
    context= {
        "form":form,
        "id": id
    }
    return render(request, 'todos/update.html', context)

def todo_item_create(request): #This function adds items to the list
    if request.method == 'POST':
        form= UpdateItem(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect('todo_list_detail', id= form.list.id)
    else:
        form = UpdateItem()
    context= {
        "form":form,
    }
    return render(request, 'todos/todo_item.html', context)
#------------------------------------------------------------------
def todo_item_update(request, id): #update the items
    update= TodoItem.objects.get(id=id)
    if request.method == 'POST':
        form = UpdateItem(request.POST, instance=update)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail',id = update.list.id)
    else:
        form = UpdateItem(instance=update)
    context={
        'form':form,
        'update_object': update,
    }
    return render(request, 'todos/todo_item_edit.html', context)

#------------------------------------------------------------------

def delete_list(request, id):
    list= get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        list.delete()
        return redirect('todo_list_list')
    else:
        form = DeleteList(TodoList, instance=list)
        context={
            'form': form
        }
        return render(request, 'todos/delete.html', context)

